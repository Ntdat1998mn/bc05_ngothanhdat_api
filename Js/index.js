const BASE_URL = "https://6363ea298a3337d9a2ec4bfc.mockapi.io";

var idEdited = null;
function fetchAllTodo() {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/todos`,
    method: "GET",
  })
    .then(function (res) {
      turnOffLoading();
      renderTodoList(res.data);
    })
    .catch(function (err) {
      turnOffLoading();
      console.log("err: ", err);
    });
}
fetchAllTodo(); // Gọi API ra
// Remove todos service
function removeTodo(idTodo) {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/todos/${idTodo}`,
    method: "DELETE",
  })
    .then(function (res) {
      turnOffLoading;
      console.log("res: ", res);
      fetchAllTodo(); // Gọi lại API
    })
    .catch(function (err) {
      turnOffLoading;
      console.log("err: ", err);
    });
}

function addTodo() {
  var data = layThongTinTuForm();
  var newTodo = {
    name: data.name,
    desc: data.desc,
    isComplete: false,
  };
  turnOnLoading;
  axios({
    url: `${BASE_URL}/todos`,
    method: "POST",
    data: newTodo,
  })
    .then(function (res) {
      turnOffLoading;
      console.log("res: ", res);
      fetchAllTodo(); // Gọi lại API
    })
    .catch(function (err) {
      turnOffLoading;
      console.log("err: ", err);
    });
}
function editTodo(idTodo) {
  turnOnLoading();

  axios({
    url: `${BASE_URL}/todos/${idTodo}`,
    method: "GET",
  })
    .then(function (res) {
      turnOffLoading();
      document.getElementById("name").value = res.data.name;
      document.getElementById("desc").value = res.data.desc;
      idEdited = res.data.id;
      renderTodoList(res.data);
    })
    .catch(function (err) {
      turnOffLoading();
      console.log("err: ", err);
    });
}
function updateTodo() {
  turnOffLoading();

  let data = layThongTinTuForm();
  axios({
    url: `${BASE_URL}/todos/${idEdited}`,
    method: "PUT",
    data: data,
  })
    .then(function (res) {
      turnOffLoading();
      fetchAllTodo();
    })
    .catch(function (err) {
      turnOffLoading();
      console.log("err: ", err);
    });
}
